import React from 'react';
import logo from './logo.svg';
import './App.css';
// import nodeSass from 'node-sass';
// import Bulma from 'bulma';
import Todos from './components/Todos';

class App extends React.Component {
  state = {
    todos: [
      {
        id : 1,
        'title' : 'Ur Trash!',
        completed : false
      },
      {
        id : 2,
        'title' : 'A little better',
        completed : false
      },
      {
        id : 3,
        'title' : 'Great',
        completed : true
      }
    ]
  }

  render() {
    return (
    <div className="App">
      <header className="App-header">
      <Todos todos={this.state.todos}/>
      
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

     
    </div>
    );
  }
}

export default App;
