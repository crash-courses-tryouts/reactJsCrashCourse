import React from 'react';
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';

export default function Todos(props) {
    const [getState, setState] = React.useState(props);

    let actionCheckbox = (id, e) => {

        console.log(id, 'ID', e);
         setState({todos: getState.todos.map(todo => {
            if (todo.id === id) {
                todo.completed = !todo.completed;
            }
            
            return todo;
        }) });
    }

    let deleteTodo = (id, e) => {
        console.log(...getState.todos.filter(todo => todo.id !== id));
        setState({todos:[...getState.todos.filter(todo => todo.id !== id)]})
    }

  return props.todos.map((todo, index) => (
  <h3> <TodoItem index={index} todo={todo} actionCheckbox={actionCheckbox} deleteTodo={deleteTodo}></TodoItem></h3>
  )); 
}

//propTypes
Todos.propTypes = {
    todos: PropTypes.array.isRequired
}
