import React, { Component } from 'react'
import PropTypes from 'prop-types';
export default function TodoItem(prop) {

    let getStyle =  () => {
        return prop.todo.completed  ? itemCompletedStyle : itemNotCompletedStyle;
    };
    const {id, title, completed } = prop.todo;


        return (
            <div>
                <input type="checkbox" onChange={prop.actionCheckbox.bind(this, id)}></input>
                <span style={getStyle()}>{prop.index+1} - {title}</span>
                <button style={delBtn} onClick={prop.deleteTodo.bind(this,id)}>x</button>
            </div>
        )
    }




//PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired
}


const itemCompletedStyle = {
    backgroundColor : 'teal',
    fontWeight : 'bolder'
}

const itemNotCompletedStyle = {
    backgroundColor : 'red'
}

const delBtn = {
    background : '#ff0000',
    color: '#fff',
    border: 'none',
    padding : '5px 9px',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right'
}
